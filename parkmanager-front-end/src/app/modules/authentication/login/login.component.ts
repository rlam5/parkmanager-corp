import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { MessageService } from 'src/app/core/services/message.service';
import { Router } from '@angular/router';
import { CustomErrorStateMatcher } from 'src/app/shared/models/custom-error-state-matcher';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /** le formulaire  */
  public form: FormGroup;
  /** errorMatcher utilisé par les formControls */
  public errorMatcher = new CustomErrorStateMatcher();

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    public router: Router,
    public message: MessageService
  ) { }

  ngOnInit() {

    this.isAlreadyAuthenticated();

    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

 /**
  * @description fonction pour envoyer l'adresse mail et le password vers le server pour l'authentification
  */
  public onSubmit(): void {

    if (this.form.valid) {
      this.authService.login(this.form.value.email, this.form.value.password).subscribe(
        data => {
          //console.log(data);
          this.router.navigate(['/parking-management']);
          this.message.displayMessage('Connexion réussie.')
        },
        err => {
          //console.log(err);
          this.form.reset();
          this.message.displayMessage('Erreur: l\'authentification a échoué !');
        }
      )
    }

  }

  /**
   * @description Vérifier si l'utilisateur est déjà authentifié
   */
  public isAlreadyAuthenticated(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/parking-management']);
    }
  }

}
