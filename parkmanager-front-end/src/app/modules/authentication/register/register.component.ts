import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { MessageService } from 'src/app/core/services/message.service';
import { Router } from '@angular/router';
import { CustomErrorStateMatcher } from 'src/app/shared/models/custom-error-state-matcher';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  /** le formulaire  */
  public form: FormGroup;
  /** errorMatcher utilisé par les formControls */
  public errorMatcher = new CustomErrorStateMatcher();

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    public message: MessageService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      lastname: ['', Validators.required],
      firstname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  /**
  * @description fonction pour envoyer les données utilisateurs vers le serveur pour l'inscription
  */
  public onSubmit(): void {

    if (this.form.valid) {
      this.authService.registerUser(
        this.form.value.lastname, 
        this.form.value.firstname,
        this.form.value.email,
        this.form.value.password
      ).subscribe(
        data => {
          //console.log(data);
          this.router.navigate(['/auth/login']);
          this.message.displayMessage('L\'inscription a réussi.');
        },
        err => {
          //console.log(err);
          this.form.reset();
          this.message.displayMessage('Erreur: l\'inscription a échoué !');
        }
      )
    }

  }

}
