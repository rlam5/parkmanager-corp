export interface User {
    id: number,
    lastname: string,
    fistname: string,
    email: string,
    role: string
}
