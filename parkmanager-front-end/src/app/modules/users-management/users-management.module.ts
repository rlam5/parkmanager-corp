import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsersManagementRoutingModule } from './users-management-routing.module';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { DeleteDialogComponent } from './dialogs/delete-dialog/delete-dialog.component';
import { AddDialogComponent } from './dialogs/add-dialog/add-dialog.component';
import { UpdateDialogComponent } from './dialogs/update-dialog/update-dialog.component';


@NgModule({
  declarations: [ManageUsersComponent, DeleteDialogComponent, AddDialogComponent, UpdateDialogComponent],
  entryComponents: [
    DeleteDialogComponent,
    AddDialogComponent,
    UpdateDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    UsersManagementRoutingModule
  ]
})
export class UsersManagementModule { }
