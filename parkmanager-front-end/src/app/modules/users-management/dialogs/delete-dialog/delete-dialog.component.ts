import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>
  ) { }

  ngOnInit() {
  }

  /**
   * @description Ferme le modal sans rien émettre
   */
  public close(): void {
    this.dialogRef.close();
  }

  /**
   * @description Ferme le modal en envoyant un booléen de valeur true
   */
  public confirm(): void {
    this.dialogRef.close(true);
  }

}
