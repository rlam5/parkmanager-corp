import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomErrorStateMatcher } from 'src/app/shared/models/custom-error-state-matcher';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Inject } from '@angular/core';

@Component({
  selector: 'app-update-dialog',
  templateUrl: './update-dialog.component.html',
  styleUrls: ['./update-dialog.component.scss']
})
export class UpdateDialogComponent implements OnInit {

  /** le formulaire  */
  public form: FormGroup;
  /** errorMatcher utilisé par les formControls */
  public errorMatcher = new CustomErrorStateMatcher();

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<UpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {

    this.form = this.fb.group({
      lastname: [this.data.lastname, Validators.required],
      firstname: [this.data.firstname, Validators.required],
      email: [this.data.email, [Validators.required, Validators.email]],
      role: [this.data.role, Validators.required]
    });

  }

  /**
   * @description Envoi des données si le formulaire est valide
   */
  public onSubmit(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    } else {
      this.dialogRef.close();
    }
  }

  /**
   * @description Annuler l'envoi des données
   */
  public onCancel(): void {
    this.dialogRef.close();
  }

}
