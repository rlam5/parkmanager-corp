import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { MessageService } from 'src/app/core/services/message.service';
import { UsersService } from '../users.service';
import { MatDialog } from '@angular/material/dialog';
import { User } from '../models/user';
import { DeleteDialogComponent } from '../dialogs/delete-dialog/delete-dialog.component';
import { AddDialogComponent } from '../dialogs/add-dialog/add-dialog.component';
import { UpdateDialogComponent } from '../dialogs/update-dialog/update-dialog.component';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {

  /** Liste des emplacements de parking */
  public users: User[];
  /** Colonnes à afficher */
  public displayedColumns = ['lastname', 'firstname', 'email', 'role', 'actions'];
  /** Source de données */
  public dataSource = new MatTableDataSource([{}]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private usersService: UsersService,
    public message: MessageService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.retrieveUsers();
  }

  /**
   * @description Récupérer la liste des utilisateurs
   */
  public retrieveUsers(): void {
    this.usersService.getUsers().subscribe(
      data => {

        console.log(data);
        this.users = data;
        this.dataSource = new MatTableDataSource(this.users);
        setTimeout(() => this.dataSource.paginator = this.paginator);

      },
      error => {
        console.error(error);
      }
    );
  }

  /**
   * @description Ajouter un nouvel utilisateur
   */
  public addNewUser(): void {

    let dialogRef = this.dialog.open(AddDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(confirm => {

      if (confirm) {
        this.usersService.registerUser(
          confirm.lastname,
          confirm.firstname,
          confirm.email,
          confirm.password,
          confirm.role
        ).subscribe(
          message => {
            console.log(message);
            this.message.displayMessage("Ajout d'un nouvel utilisateur réussi.");
            this.retrieveUsers();
          },
          err => {
            console.error(err);
            this.message.displayMessage("Erreur: impossible d'ajouter un nouvel utilisateur.");
          }
        )
      }
    });

  }

  /**
   * @description Mettre à jour un utilisateur
   */
  public updateUser(
    index: number, 
    userid: number, 
    lastname: string, 
    firstname: string,
    email: string,
    role: string
  ): void {

    let dialogRef = this.dialog.open(UpdateDialogComponent, {
      width: '500px',
      data: {
        lastname: lastname,
        firstname: firstname,
        email: email,
        role: role
      }
    });

    dialogRef.afterClosed().subscribe(confirm => {
      console.log(confirm);

      if (confirm) {
        this.usersService.updateUser(
          userid,
          confirm.lastname,
          confirm.firstname,
          confirm.email,
          confirm.role
        ).subscribe(
          message => {
            console.log(message);
            this.message.displayMessage("Mise à jour données utilisateur réussie");
            this.retrieveUsers();
          },
          err => {
            console.error(err);
            this.message.displayMessage("Erreur: la mise à jour des données utilisateur a échoué");
          }
        )
      }
    });

  }

  /**
   * @description Supprimer un utilisateur
   * @param {number} index index dans le tableau
   * @param {number} userid identifiant de l'utilisateur
   */
  public deleteUser(index: number, userid: number): void {

    // console.log('index', index);
    // console.log('userid', userid);

    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.usersService.deleteUser(userid).subscribe(
          message => {
            console.log(message);
            this.message.displayMessage("Suppression de l'utilisateur réussie.");
            this.retrieveUsers();
          },
          error => {
            console.error(error);
            this.message.displayMessage("Erreur: impossible de supprimer l'utilisateur.");
          }
        );
      }
    });

  }

  /**
   * @description Actualiser la liste des utilisateurs
   */
  public refresh(): void {
    this.retrieveUsers();
    setTimeout(() => this.message.displayMessage('Actualisation terminée'));
  }


}
