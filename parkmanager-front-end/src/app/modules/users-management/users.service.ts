import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  /** URL vers les APIs back-end */
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private authService: AuthService) { }

  /**
   * @description Récupérer la liste des utilisateurs à partir du serveur
   */
  public getUsers(): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.get(this.baseUrl + 'api/manage-users/', {headers: {Authorization: token}});
  }

  /**
   * @description Supprimer un utilisateur à partir du serveur
   * @param {number} userid identifiant de l'utilisateur
   */
  public deleteUser(userid: number): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.delete(this.baseUrl + 'api/manage-users/'+ userid, {headers: {Authorization: token}});
  }

  /**
   * @description Enregistrer un nouvel utilisateur
   * @param {string} lastname nom
   * @param {string} firstname prénom
   * @param {string} email email
   * @param {string} password mot de passe
   * @param {string} role role de l'utilisateur
   */
  public registerUser(lastname: string, firstname: string, email: string, password: string, role: string): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.post(this.baseUrl + 'api/manage-users/', {
      lastname: lastname,
      firstname: firstname,
      email: email, 
      password: password,
      role: role
    }, {headers: {Authorization: token}});
  }

  /**
   * @description Supprimer un utilisateur à partir du serveur
   * @param {number} userid identifiant de l'utilisateur
   */
  public updateUser(userid: number, lastname: string, firstname: string, email: string, role: string): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.put(this.baseUrl + 'api/manage-users/', {
      id: userid,
      lastname: lastname,
      firstname: firstname,
      email: email, 
      role: role
    }, {headers: {Authorization: token}});
  }
}
