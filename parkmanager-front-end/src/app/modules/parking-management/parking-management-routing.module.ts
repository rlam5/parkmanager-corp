import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuardService as AdminGuard } from 'src/app/core/guards/admin-guard.service';
import { ParkingOverviewComponent } from './parking-overview/parking-overview.component';
import { ManageParkingSlotsComponent } from './manage-parking-slots/manage-parking-slots.component';

const routes: Routes = [
  {
    path: '',
    component: ParkingOverviewComponent
  },
  {
    path: 'manage-parking-slots',
    component: ManageParkingSlotsComponent,
    canActivate: [AdminGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParkingManagementRoutingModule { }
