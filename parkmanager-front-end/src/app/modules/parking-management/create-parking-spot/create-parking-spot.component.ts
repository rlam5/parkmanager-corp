import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomErrorStateMatcher } from 'src/app/shared/models/custom-error-state-matcher';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

@Component({
  selector: 'app-create-parking-spot',
  templateUrl: './create-parking-spot.component.html',
  styleUrls: ['./create-parking-spot.component.scss']
})
export class CreateParkingSpotComponent implements OnInit {

  /** le formulaire  */
  public form: FormGroup;
  /** errorMatcher utilisé par les formControls */
  public errorMatcher = new CustomErrorStateMatcher();
  /** Etages disponibles */
  public floors: number[] = [];
  /** Emplacements disponibles */
  public locations: number[] = [];

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CreateParkingSpotComponent>
  ) { }

  ngOnInit() {

    for (let i= 1; i < 5; i ++) {
      this.floors.push(i);
    }

    for (let i= 1; i < 31; i ++) {
      this.locations.push(i);
    }

    this.form = this.fb.group({
      number: ['', Validators.required],
      floor: ['', Validators.required]
    });
  }

  /**
   * @description Envoi des données si le formulaire est valide
   */
  public onSubmit(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    } else {
      this.dialogRef.close();
    }
  }

}
