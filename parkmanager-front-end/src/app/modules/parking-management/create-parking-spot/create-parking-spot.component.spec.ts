import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateParkingSpotComponent } from './create-parking-spot.component';

describe('CreateParkingSpotComponent', () => {
  let component: CreateParkingSpotComponent;
  let fixture: ComponentFixture<CreateParkingSpotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateParkingSpotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateParkingSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
