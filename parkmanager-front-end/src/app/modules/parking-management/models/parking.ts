export interface Parking {
    id: number,
    number: number,
    floor: number,
    occupied: boolean,
    userid: number
}
