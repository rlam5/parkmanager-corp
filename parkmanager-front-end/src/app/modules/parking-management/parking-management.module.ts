import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ParkingManagementRoutingModule } from './parking-management-routing.module';
import { CreateParkingSpotComponent } from './create-parking-spot/create-parking-spot.component';
import { ParkingOverviewComponent } from './parking-overview/parking-overview.component';
import { ManageParkingSlotsComponent } from './manage-parking-slots/manage-parking-slots.component';
import { ConfirmationDialogComponent } from './dialogs/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [CreateParkingSpotComponent, ParkingOverviewComponent, ManageParkingSlotsComponent, ConfirmationDialogComponent],
  entryComponents: [CreateParkingSpotComponent, ConfirmationDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ParkingManagementRoutingModule
  ]
})
export class ParkingManagementModule { }
