import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ParkingService {

  /** URL vers les APIs back-end */
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private authService: AuthService) { }

  /**
   * @description Récupérer les emplacements parking à partir du serveur
   */
  public getParkingSlots(): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.get(this.baseUrl + 'api/manage-parking/', {headers: {Authorization: token}});
  }

  /**
   * @description Ajouter un nouvel emplacement de parking vers le serveur
   * @param {number} number le numéro d'emplacement
   * @param {number} floor le numéro d'étage
   */
  public addNewParkingSlot(number: number, floor: number) : Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.post(this.baseUrl + 'api/manage-parking/parking-slot', {number: number, floor: floor}, {headers: {Authorization: token}});
  }

  /**
   * @description Récupérer les emplacements parking disponibles à partir du serveur
   */
  public getAvailableParkingSlots(): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.get(this.baseUrl + 'api/manage-parking/available-parking', {headers: {Authorization: token}});
  }

  /**
   * @description Sélectionner un emplacement de parking à partir du serveur
   */
  public selectParkingSlot(userid: number, floor: number, parkingNumber: number): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.put(this.baseUrl + 'api/manage-parking/select-parking', {
      id: userid,
      floor: floor,
      number: parkingNumber
    }, {headers: {Authorization: token}});
  }

  /**
   * @description Récupérer l'emplacement de l'utilisateur
   */
  public getUserParkingSlot(userid: number): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.get(this.baseUrl + 'api/manage-parking/select-parking/' + userid, {headers: {Authorization: token}});
  }

  /**
   * @description Supprimer l'emplacement de l'utilisateur
   */
  public deleteUserParkingSlot(userid: number): Observable<any> {
    /** token JWT */
    let token = this.authService.getToken();

    return this.http.delete(this.baseUrl + 'api/manage-parking/select-parking/' + userid, {headers: {Authorization: token}});
  }

}
