import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { User } from '../models/user';
import { Parking } from '../models/parking';
import { AuthService } from 'src/app/core/services/auth.service';
import { ParkingService } from '../parking.service';
import { MessageService } from 'src/app/core/services/message.service';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { ConfirmationDialogComponent } from '../dialogs/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-parking-overview',
  templateUrl: './parking-overview.component.html',
  styleUrls: ['./parking-overview.component.scss']
})
export class ParkingOverviewComponent implements OnInit {

  /** Informations de l'utilisateur */
  public currentUser: User = {
    id: 0,
    lastname: '',
    firstname: '',
    email: '', 
    role: ''
  };
  /** Emplacement de l'utilisateur */
  public currentParking: Parking = {
    id: 0,
    number: 0,
    floor: 0,
    occupied: false,
    userid: 0
  };
  /** Savoir si l'utilisateur a un parking */
  hasParking: boolean = false;
  /** Liste des emplacements de parking disponibles */
  public availableParkingSlots: Parking[] = [];
  /** Colonnes à afficher */
  public displayedColumns = ['number', 'floor', 'occupied', 'action'];
  /** Source de données */
  public dataSource = new MatTableDataSource([{}]);
  /** FormControl pour le filtre par étage */
  public floorFilter = new FormControl('');
  /** Stockage valeur filtrée */
  public filterValues = {
    floor: ''
  };

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private authService: AuthService,
    private parkingService: ParkingService,
    public message: MessageService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
      let userInfo = this.authService.getUserInfo();

      if (userInfo) {
        this.currentUser.id = userInfo.id;
        this.currentUser.lastname = userInfo.lastname;
        this.currentUser.firstname = userInfo.fistname;
        this.currentUser.email = userInfo.email;
        this.currentUser.role = userInfo.role;
      }

      // console.log(this.currentUser);

      this.retrieveAvailableParkingSlots();
      this.getUserParkingSlot();

      this.floorFilter.valueChanges
      .subscribe(
        floor => {
          this.filterValues.floor = floor;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

  }

  /**
   * @description Récupérer les emplacements de parking disponibles
   */
  public retrieveAvailableParkingSlots(): void {
    this.parkingService.getAvailableParkingSlots().subscribe(
      parkingSlots => {
        this.availableParkingSlots = parkingSlots;
        this.dataSource = new MatTableDataSource(this.availableParkingSlots);
        this.dataSource.filterPredicate = this.tableFilter();
        setTimeout(() => this.dataSource.paginator = this.paginator);
      },
      err => {
        console.error(err);
        this.message.displayMessage("Erreur: la récupération des emplacements a échoué.");
      }
    );
  }

  /**
   * @description Actualiser la liste des emplacements de parking disponibles
   */
  public refresh(): void {
    this.retrieveAvailableParkingSlots();
    setTimeout(() => this.message.displayMessage('Actualisation terminée'));
  }

  /**
   * @description Sélectionner un emplacement de parking
   * @param {number} parkingNumber numéro emplacement
   * @param {number} floor numéro étage
   */
  public selectParkingSlot(parkingNumber: number, floor: number): void {

    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(confirm => {

      if (confirm) {

        // console.log(parkingNumber);
        // console.log(floor);

        this.parkingService.selectParkingSlot(this.currentUser.id, floor, parkingNumber).subscribe(
          message => {
            console.log(message);
            this.message.displayMessage("Emplacement parking sélectionné.");
            this.retrieveAvailableParkingSlots();
            this.getUserParkingSlot();
          },
          err => {
            console.error(err);
            this.message.displayMessage("Erreur: la sélection de l'emplacement a échoué.");
          }
        )
        
      }

    });
  }

  /**
   * @description Filtre personnalisé
   */
  public tableFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
      let searchTerms = JSON.parse(filter);
      // console.log(searchTerms);
      // console.log(data);
      return data.floor == searchTerms.floor;
    }
    return filterFunction;
  }

  /**
   * @description Récupérer l'emplacement de parking de l'utilisateur
   */
  public getUserParkingSlot(): void {

    this.hasParking = false;
    this.parkingService.getUserParkingSlot(this.currentUser.id).subscribe(
      parkingSlot => {
        console.log(parkingSlot);
        this.currentParking = parkingSlot;
        this.hasParking = true;
      },
      err => {
        console.error(err);
      }
    )

  }

  /**
   * @description Libérer l'emplacement de parking de l'utilisateur
   */
  public deleteUserParkingSlot(): void {

    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(confirm => {

      if (confirm) {

        this.parkingService.deleteUserParkingSlot(this.currentUser.id).subscribe(
          message => {
            console.log(message);
            this.getUserParkingSlot();
            this.message.displayMessage("Place de parking libéré.");
          }, err => {
            console.error(err);
            this.message.displayMessage("Erreur: Impossible de libérer la place.")
          }
        );

      }

    });

  }

}
