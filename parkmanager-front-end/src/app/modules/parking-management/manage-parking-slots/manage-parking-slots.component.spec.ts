import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageParkingSlotsComponent } from './manage-parking-slots.component';

describe('ManageParkingSlotsComponent', () => {
  let component: ManageParkingSlotsComponent;
  let fixture: ComponentFixture<ManageParkingSlotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageParkingSlotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageParkingSlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
