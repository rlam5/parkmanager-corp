import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { MessageService } from 'src/app/core/services/message.service';
import { ParkingService } from '../parking.service';
import { Parking } from '../models/parking';
import { MatDialog } from '@angular/material/dialog';
import { CreateParkingSpotComponent } from '../create-parking-spot/create-parking-spot.component';

@Component({
  selector: 'app-manage-parking-slots',
  templateUrl: './manage-parking-slots.component.html',
  styleUrls: ['./manage-parking-slots.component.scss']
})
export class ManageParkingSlotsComponent implements OnInit {

  /** Liste des emplacements de parking */
  public parkings: Parking[];
  /** Colonnes à afficher */
  public displayedColumns = ['number', 'floor', 'occupied', 'userid'];
  /** Source de données */
  public dataSource = new MatTableDataSource([{}]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private parkingService: ParkingService,
    public message: MessageService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.retrieveParkingSlots()
  }

  /**
   * @description Récupérer la liste des emplacements du parking
   */
  public retrieveParkingSlots(): void {
    this.parkingService.getParkingSlots().subscribe(
      data => {

        console.log(data);
        this.parkings = data;
        this.dataSource = new MatTableDataSource(this.parkings);
        setTimeout(() => this.dataSource.paginator = this.paginator);

      },
      error => {
        console.error(error);
      }
    );
  }

  // public applyFilter(filterValue: string) {

  //   filterValue = filterValue.trim(); // Remove whitespace
  //   filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  //   this.dataSource.filter = filterValue;

  // }

  /**
   * @description Actualiser la liste des emplacements de parking
   */
  public refresh(): void {
    this.retrieveParkingSlots();
    setTimeout(() => this.message.displayMessage('Actualisation terminée'));
  }

  /**
   * @description Ajouter un nouvel emplacement de parking
   */
  public addNewSlot(): void {
    let dialogRef = this.dialog.open(CreateParkingSpotComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(confirm => {

      if (confirm) {

        console.log('confirm value', confirm);

        if (this.checkParkingSlotExistence(confirm.floor, confirm.number)) {
          this.message.displayMessage("Erreur: l'emplacement existe déjà");
          return;
        }

        this.parkingService.addNewParkingSlot(confirm.number, confirm.floor).subscribe(
          message => {
            console.log(message);
            this.message.displayMessage("L'ajout d'un nouvel emplacement réussi");
            // Actualisation des données
            this.retrieveParkingSlots();
          },
          err => {
            console.error("add parking lot error", err);
            this.message.displayMessage("L'ajout d'un nouvel emplacement a échoué");
          }
        );
      }

    });
  }

  /**
   * @description Vérifier en amont si l'emplacement existe dejà
   */
  public checkParkingSlotExistence(targetFloor: number, targetNumber: number): boolean {

    if (this.parkings.length == 0) {
      return false;
    }

    /** Contient l'emplacement existant */
    let filteredFloor = this.parkings.filter(element => element.floor == targetFloor);
    filteredFloor = filteredFloor.filter(element => element.number == targetNumber);

    // console.log(filteredFloor);
    if (filteredFloor.length > 0) {
      return true;
    } else {
      return false;
    }

  }

}
