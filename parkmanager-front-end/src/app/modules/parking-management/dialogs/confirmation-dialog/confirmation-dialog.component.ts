import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>
  ) { }

  ngOnInit() {
  }

  /**
   * @description Ferme le modal sans rien émettre
   */
  public close(): void {
    this.dialogRef.close();
  }

  /**
   * @description Ferme le modal en envoyant un booléen de valeur true
   */
  public confirm(): void {
    this.dialogRef.close(true);
  }

}
