import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

/**
 * @description Service AdminGuardService qui permet de protéger les routes qui ne sont accessibles que par l'administrateur
 */
@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate{

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  /**
   * @description Retourne un boolean pour savoir si une route protégée peut être activée
   * en utilisant la fonction getUserRole() du service authenticationService. Si true, alors il pourra 
   * naviguer dans la route protégée sinon il est redirigé vers /parking-management
   */
  public canActivate(): boolean {
    console.log(this.authService.getUserRole());
    if (this.authService.getUserRole() !== 'admin') {
      this.router.navigate(['/parking-management']);
      return false;
    } else {
      return true;
    }

    
  }
}
