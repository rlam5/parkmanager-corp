import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

/**
 * @description Service AuthGuardService qui permet de protéger les routes qui ne sont accessibles que pour un utilisateur authentifié
 */
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  /**
   * @description Retourne un boolean pour savoir si une route protégée peut être activée
   * en utilisant la fonction isAuthenticated() du service authenticationService. Si true, alors il pourra 
   * naviguer dans la route protégée sinon il est redirigé vers /auth/login
   */
  public canActivate(): boolean {
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/auth/login']);
      return false;
    }

    return true;
  }

}
