import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  /** Snack Bar configuration */
  private config: MatSnackBarConfig;

  constructor(public messageSnackBar: MatSnackBar) {
    this.config = new MatSnackBarConfig();
    this.config.duration = 3000;
    this.config.panelClass = ['font-size:20px;'];
  }

  /**
   * @description Affiche un message dans un MatSnackBar composant
   * @param {string} message contient le message à afficher
   */
  public displayMessage(message: string): void {
    this.messageSnackBar.open(message, 'OK', this.config);
  }

}
