import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';

/**
 * interface utilisée pour récupérer le token envoyé par le serveur
 */
export interface Token {
  /** le token JWT est une chaîne de caractères */
  token: string
}

/**
 * @description Service AuthService qui s'occupe de l'authentification (connexion et inscription)
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /** URL vers les APIs back-end */
  private baseUrl = environment.baseUrl;
  /** le token JWT */
  private token: string;

  constructor(private router: Router, private http: HttpClient) { }

  /**
   * @description Stocker le token JWT dans localStorage
   * @param {string} token le token JWT
   */
  private saveToken(token: string): void {
    localStorage.setItem('parkmanager-corp-token', token);
    this.token = token;
  }

  /**
   * @description Retourner la valeur du token JWT
   * @returns {string} la valeur du token JWT
   */
  public getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('parkmanager-corp-token');
    }

    return this.token;
  }

  /**
   * @description Récupérer la date d'expiration du token JWT
   * @param {string} token le token JWT
   * @returns {Date} la date d'expiration
   */
  private getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) return null;

    const date = new Date(0); 
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  /**
   * @description Savoir si le token JWT a expiré
   * @param {string} token le token JWT
   * @returns {boolean}
   */
  private isTokenExpired(token?: string): boolean {
    if(!token) token = this.getToken();
    if(!token) return true;

    const date = this.getTokenExpirationDate(token);
    if(date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }

  /**
   * @description Savoir si un utilisateur est authentifié
   * @returns {boolean}
   */
  public isAuthenticated(): boolean {
    return !this.isTokenExpired(this.getToken());
    // return true; // Décommenter cette ligne pour renvoyer toujours true
  }

  /**
   * @description S'authentifier en envoyant les informations user vers la base de données
   * elle envoie une requête HTTP POST et attend en retour un token JWT
   * @param {string} email email
   * @param {string} password mot de passe
   */
  public login(email: string, password: string): Observable<any> {

    return this.http.post(this.baseUrl + 'api/authentication/login', {email: email, password: password})
      .pipe(
        map((response: Token) => {
          if (response.token) {
            this.saveToken(response.token)
          }
        })
      );
      
  }

  /**
   * @description Se déconnecter. On supprime le token JWT de localStorage et on redirige l'utilisateur vers /auth/login
   */
  public logout(): void {

    this.token = '';
    window.localStorage.removeItem('parkmanager-corp-token');
    this.router.navigate(['/auth/login']);

  }

  /**
   * @description Enregistrer un nouvel utilisateur
   * @param {string} lastname nom
   * @param {string} firstname prénom
   * @param {string} email email
   * @param {string} password mot de passe
   */
  public registerUser(lastname: string, firstname: string, email: string, password: string): Observable<any> {
    return this.http.post(this.baseUrl + 'api/authentication/register', {
      lastname: lastname,
      firstname: firstname,
      email: email, 
      password: password
    });
  }

  /**
   * @description Récupérer les informations utilisateur dans le token JWT
   */
  public getUserInfo(): any {
    if (!this.token) return;

    const decoded = jwt_decode(this.token);

    return {
      id: decoded.userid,
      lastname: decoded.lastname,
      firstname: decoded.firstname,
      email: decoded.email,
      role: decoded.role
    };

  }

  /**
   * @description Récupérer le rôle de l'utilisateur dans le token JWT
   */
  public getUserRole(): any {
    if (!this.token) return;

    const decoded = jwt_decode(this.token);

    return decoded.role;
  }

}
