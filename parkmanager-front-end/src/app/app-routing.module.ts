import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'src/app/core/guards/auth-guard.service';
import { NavBarComponent } from './shared/components/nav-bar/nav-bar.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './modules/authentication/authentication.module#AuthenticationModule'
  },
  {
    path: '',
    component: NavBarComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'parking-management',
        loadChildren: './modules/parking-management/parking-management.module#ParkingManagementModule'
      },
      {
        path: 'users-management',
        loadChildren: './modules/users-management/users-management.module#UsersManagementModule'
      }
    ]
  },
  {
    path: '404',
    component: NotFoundComponent,
  },
  {
    path: '**',
    redirectTo: '/404',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
