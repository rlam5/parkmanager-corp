import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  /** Store screen width */
  public screenWidth: number;
  /** Stocker le role de l'utilisateur */
  public userRole: string = "";

  constructor(public authService: AuthService) {
    this.screenWidth = window.innerWidth;

    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    this.userRole = this.authService.getUserRole();
    console.log(this.userRole);
  }

  /**
   * @description Se déconnecter en supprimant le token JWT dans le localStorage, redirection vers la page de login
   */
  public logout(): void {
    this.authService.logout();
  }

}
