# Projet Parkmanager Corp

Création d'une application web pour la gestion d'un parking. 
Elle gère l'authentification des utilisateurs, permet de sélectionner un emplacement de parking disponible pour un utilisateur.
L'administrateur peut gérer son parking et son parc d'utilisateurs.

La stack utilisée pour cette application est la suivante:

_ **Angular 8** pour la front-end (interface graphique)

_ **Node JS** pour la partie back-end (partie serveur). La partie serveur est développé avec le framework Express JS

_ **MySQL** une base de données côté serveur pour stocker les données utlisateurs et parking

## Nota bene

Dans le cadre de ce projet, les mots de passe sont stockés en clair et ne sont pas hashés.

La configuration de la base de données MySQL en local sont affichées en clair dans le répertoire.

Les identifiants de connexion pour l'administrateur sont:

email: *master@gmail.com*

password: *master*

## Prérequis

Installer **Node JS** sur votre PC (+V10.15.0), **Node Package Manager** (+V6.4.1), **MySQL** et **Git**.

Configurer les paramètres de **votre base de données MySQL** dans le fichier ./parkmanager-back-end/config/config.js

notamment les paramètres host, port, user et pwd. Le paramètre database doit rester le même.

Il est impératif de lancer le **script sql** présent dans le fichier ./parkmanager-back-end/database/park_manager_database_structure.sql sur **votre base de données MySQL**. Ce script initialise la base de données de l'application

## Installation

Pour installer toutes les dépendances, mettez vous à la racine du projet ./ puis faites:

à partir de ./ `cd parkmanager-front-end` puis `npm install` pour installer les dépendances liées à la partie front-end **Angular 8**

à partir de ./ `cd parkmanager-back-end` puis `npm install` pour installer les dépendances liées à la partie back-end **Node JS**

## Serveur de développement

Pour lancer le serveur de développement pour la partie front-end développée en **Angular 8**, faites:

à partir de ./ `cd parkmanager-front-end` `ng serve --open`

Le lien s'ouvrira sur votre navigateur par défaut à l'adresse: `http://localhost:4200/`

Le code source de la partie front-end **Angular 8** se trouve dans le répertoire ./parkmanager-front-end.

Pour le serveur de développement **Express JS** (avec **Node JS**) pour la partie back-end, faites:

à partir de ./ `cd parkmanager-back-end` `npm start`

Le serveur de développement se trouve à l'adresse: `http://localhost:3000/`

Pour servir la partie front-end **Angular 8** à partir du serveur **Express JS**, il faut d'abord compiler le code **Angular 8**, faites:

1) à partir de ./ `cd parkmanager-front-end` `ng build --prod`

2) copier le dossier **parkmanager-front-end/dist** dans le dossier **parkmanager-back-end**

3) en navigant à l'adresse `http://localhost:3000/`, vous tomberez sur l'interface graphique développée en **Angular 8**

La branche master contient déjà la partie **Angular 8** compilée (**parkmanager-front-end/dist**). Vous pouvez lancer directement le serveur **Express JS**.

## Generation documentation technique

à partir de ./ `cd parkmanager-front-end`, `npm run compdoc` pour générer la documentation technique **Angular 8**, la documentation sera générée dans le répertoire **./documentation/angular**

à partir de ./ `cd parkmanager-back-end`, `npm run jsdoc` pour générer la documentation technique **Node JS**, la documentation sera générée dans le répertoire **./documentation/node-js**

## Lancement des tests unitaires des APIs

à partir de ./ `cd parkmanager-back-end`, `npm run test` pour lancer les tests.
