/**
 * @module parking-management
 * @description Module pour gérer les opérations liées la gestion du parking
 */

const db = require('../libs/db');

/**
 * @description Récupérer les emplacements du parking
 * @param {boolean} onlyAvailable filtrage sur emplacements disponibles seulement
 */
async function getParkingSlots(onlyAvailable) {
    return new Promise(async(resolve, reject) => {
        try {

            let parkingSlots;

            if (onlyAvailable) {
                await db.getParkingSlots(onlyAvailable).then(results => parkingSlots = results);
                resolve(parkingSlots);
            } else {
                await db.getParkingSlots().then(results => parkingSlots = results);
                resolve(parkingSlots);
            }

        } catch (err) {
            reject(err);
        }
    });
}

/**
 * @description Ajouter un nouvel emplacement de parking
 * @param {number} number emplacement du parking
 * @param {number} floor étage
 */
async function insertParkingSlot(number, floor) {
    return new Promise(async(resolve, reject) => {
        try {
            await db.insertNewParkingSlot(number, floor)
                .then(() => console.log("Inserted a new parking slot"))
                .catch(err => {
                    console.error("Insert parking slot failed", err);
                    throw(err);
                });

            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

/**
 * @description Récupérer l'emplacement parking d'un utilisateur
 * @param {number} userid identifiant de l'utilisateur
 */
async function getUserParkingSlot(userid) {
    return new Promise(async(resolve, reject) => {
        try {

            let userParkingSlot;

            await db.getUserParkingSlot(userid).then(parkingSlot => userParkingSlot = parkingSlot);
            resolve(userParkingSlot);

        } catch (err) {
            reject(err);
        }
    });
}

/**
 * @description Selectionner un emplacement parking pour un utilisateur
 * @param {number} userid identifiant de l'utilisateur
 * @param {number} floor étage
 * @param {number} number emplacement du parking
 */
async function selectParkingSlot(userid, floor, number) {
    return new Promise(async(resolve, reject) => {
        try {
            await db.selectParkingSlot(userid, floor, number)
                .then(() => console.log("Select a parking slot for a user successful"))
                .catch(err => {
                    console.error("Select a parking slot failed", err);
                    throw(err);
                });

            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

/**
 * @description Supprimer l'emplacement parking d'un utilisateur
 * @param {number} userid identifiant de l'utilisateur
 */
async function deleteUserParkingSlot(userid) {
    return new Promise(async(resolve, reject) => {
        try {
            await db.deleteUserParkingSlot(userid).then();
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

// getParkingSlots()
//     .then(() => console.log("ok"))
//     .catch(err => console.error('final', err));

// insertParkingSlot(40, 3)
//     .then(() => console.log("ok"))
//     .catch(err => console.error('insert Parking Slot op failed', err));

module.exports.getParkingSlots = getParkingSlots;
module.exports.insertParkingSlot = insertParkingSlot;
module.exports.getUserParkingSlot = getUserParkingSlot;
module.exports.selectParkingSlot = selectParkingSlot;
module.exports.deleteUserParkingSlot = deleteUserParkingSlot;