/**
 * @module users-management
 * @description Module pour gérer les opérations liées la gestion des utilisateurs
 */

const db = require('../libs/db');

/**
 * @description Récupérer la liste des utilisateurs
 */
async function getUsers() {
    return new Promise(async(resolve, reject) => {
        try {

            let usersList;
            await db.getUsers().then(results => usersList = results);
            resolve(usersList);

        } catch (err) {
            reject(err);
        }
    });
}

/**
 * @description Enregistre un nouvel utilisateur dans la base de données
 * On vérifie si l'email n'existe pas dans la base de données
 * @param {string} lastname nom
 * @param {string} firstname prénom
 * @param {string} email email
 * @param {string} password mot de passe
 * @param {string} role rôle de l'utilisateur
 */
async function registerUser(lastname, firstname, email, password, role) {

    return new Promise(async(resolve, reject) => {

        // vérifier l'existence de l'email
        await db.checkEmailExistence(email)
            .then(() => {
                console.log("email does not exist, will register new user");
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });

        await db.insertUser(lastname, firstname, email, password, role)
            .then(() => {
                console.log("insert new user in database successful");
                resolve();
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });

    });

}

/**
 * @description Supprimer un utilisateur
 * @param {string} userid identifiant de l'utilisateur
 */
async function deleteUser(userid) {
    return new Promise(async(resolve, reject) => {
        try {
            await db.deleteUser(userid);
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

/**
 * @description Mettre à jour un utilisateur
 * @param {string} userid l'identifiant de l'utilisateur
 * @param {string} lastname nom
 * @param {string} firstname prénom
 * @param {string} email email
 * @param {string} role role
 */
async function updateUser(userid, lastname, firstname, email, role) {
    return new Promise(async(resolve, reject) => {
        try {
            await db.updateUser(userid, lastname, firstname, email, role);
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

module.exports.getUsers = getUsers;
module.exports.registerUser = registerUser;
module.exports.deleteUser = deleteUser;
module.exports.updateUser = updateUser;