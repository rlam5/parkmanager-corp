/**
 * @module authentication
 * @description Module pour gérer les opérations liées à l'authentification des utilisateurs
 */

const db = require('../libs/db');
const jwt = require('jsonwebtoken');
const config = require('../config/config');

/**
 * @description Enregistre un nouvel utilisateur dans la base de données
 * On vérifie si l'email n'existe pas dans la base de données
 * @param {string} lastname nom
 * @param {string} firstname prénom
 * @param {string} email email
 * @param {string} password mot de passe
 */
async function registerUser(lastname, firstname, email, password) {

    return new Promise(async(resolve, reject) => {

        // vérifier l'existence de l'email
        await db.checkEmailExistence(email)
            .then(() => {
                console.log("email does not exist, will register new user");
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });

        await db.insertUser(lastname, firstname, email, password)
            .then(() => {
                console.log("insert new user in database successful");
                resolve();
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });

    });

}

/**
 * @description Authentifie un utilisateur et génère un token JWT pour l'utilisateur lorsque l'authentification a réussi
 * @param {string} email email
 * @param {string} password mot de passe
 */
async function authenticateUser(email, password) {
    return new Promise(async(resolve, reject) => {

        await db.checkUserCredentials(email, password)
                    .then(userInfo => {
                        // console.log(userInfo);

                        // Générer un token JWT
                        let expiry = new Date();
                        expiry.setDate(expiry.getDate() + 1);
                      
                        let token = jwt.sign({
                            userid: userInfo.id,
                            lastname: userInfo.lastname,
                            firstname: userInfo.firstname,
                            email: userInfo.email,
                            role: userInfo.role,
                            exp: parseInt(expiry.getTime() / 1000),
                        }, config.secret); 
                    
                        resolve(token);
                    })
                    .catch(err => {
                        console.error(err);
                        reject(err);
                    });
    });
}

// registerUser('lam', 'remi', 'lam_remiii@hotmail.fr', 'password')
//     .then(() => console.log('OK'))
//     .catch(err => console.error('err', err));

// authenticateUser('master@gmail.com','master')
//     .then(token => console.log(token))
//     .catch(err => console.error('err', err));

module.exports.registerUser = registerUser;
module.exports.authenticateUser = authenticateUser;