/**
 * @module check-authorization
 * @description Module pour vérifier le token JWT
 */

const config = require('../config/config');
const jwt = require('jsonwebtoken');

/**
 * @description Vérifier la validité du token JWT pour autoriser l'accès aux autres routes
 * @param {object} req l'objet requête
 * @param {object} res l'objet réponse
 * @param {callback} next callback
 */
async function checkAuthorization(req, res, next) {
    
    console.log('check authorization');
    let token = req.headers['authorization'];

    // Si absence de token
    if (!token) {
        console.error('authorization error: access denied (no token provided)');
        return res.status(403).json({message: 'No token provided' });
    }

    // Vérification de la validité du token JWT
    jwt.verify(token, config.secret, function(err, decoded) {

        // Si erreur ou absence des propriétés exp, userid et role
        if (err || !decoded.exp || !decoded.userid || !decoded.role) {
            console.log('authorization error: access denied (Failed to authenticate token)');
            return res.status(403).json({message: 'Failed to authenticate token.'});  
        }

        if (decoded.exp <  parseInt(new Date().getTime() / 1000)) {
            console.log('authorization error: token has expired');
            return res.status(403).json({message: 'token has expired'}); 
        }

        // Si le token JWT est valide, on rajoute le token JWT décodé à l'objet req
        req.decoded = decoded;
        console.log('authorization success: allow access to API');
        next();

    });

};

/**
 * @description Vérifier si le role est celui de 'admin'
 * @param {object} req l'objet requête
 * @param {object} res l'objet réponse
 * @param {callback} next callback
 */
async function isAdmin(req, res, next) {

    if (req.decoded.role !== 'admin') {
        console.log('authorization error: user not allowed');
        return res.status(403).json({message: 'user not allowed'}); 
    }

    console.log('authorization success: allow access to admin API');
    next();

}

module.exports.checkAuthorization = checkAuthorization;
module.exports.isAdmin = isAdmin;