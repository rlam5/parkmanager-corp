/**
 * @module db
 * @description Module pour gérer les opérations liées à la base de données
 */

const mysql = require('mysql');
const config = require('../config/config');

/** Connection to MYSQL database */
let connection;

/**
 * @description Connexion vers la base de données MYSQL
 */
async function connectToDatabase() {
    return new Promise((resolve, reject) => {
        connection = mysql.createConnection({
            // host: process.env.SPEED_DB_HOST,
            // user: process.env.SPEED_DB_USER,
            // password: process.env.SPEED_DB_PASS,
            // database: process.env.SPEED_DB_NAME
            host: config.mysql.host,
            port: config.mysql.port,
            user: config.mysql.user,
            password: config.mysql.pwd,
            database: config.mysql.database
        });

        connection.connect((err) => {
            if (err) {
                reject('MYSQL CONNECTION ERROR:' + err.code);
            } else {
                resolve('CONNECTION TO MYSQL DATABASE SUCCESSFUL');
            }
        });
    });
}

/**
 * @description Déconnexion vers la base de données MYSQL
 */
async function disconnectToDatabase() {
    return new Promise((resolve) => {
        connection.end((err) => {
            if (err) {
                console.error('DECONNECTION ERROR', err);
            }
            resolve('DECONNECTION TO MYSQL DATABASE SUCCESSFUL');
        });
    });
}

/**
 * @description Vérifier l'existence d'un email
 * @param {string} email email à vérifier
 */
function checkEmailExistence(email) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        let query = "SELECT email FROM users WHERE email = ? ";
    
        connection.query(query, email,  async(err, results, fields) => {
            // console.error(err);
            // console.log(results);
            // console.log(fields);

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);
            
            // Rejects if results exist
            if (results.length > 0) reject("mysql error: email already exists");

            resolve();
        });
    });
}

/**
 * @description Insérer un nouvel utilisateur dans la base de données
 * @param {string} lastname nom
 * @param {string} firstname prénom
 * @param {string} email email
 * @param {string} password mot de passe
 * @param {string} role role de l'utilisateur
 */
function insertUser(lastname, firstname, email, password, role) {
    return new Promise(async(resolve,reject) => {

        // Vérifie si le role est définie
        let targetRole = role ? role : 'user';

        await connectToDatabase();

        let insertQuery = "INSERT INTO users (lastname, firstname, email, password, role) VALUES (?, ?, ?, ?, ?)";
    
        connection.query(insertQuery, [lastname, firstname, email, password, targetRole],  async(err, results, fields) => {
            // console.error(err);
            // console.log(results);
            // console.log(fields);

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);
            
            resolve();
        });
    });
}

/**
 * @description Vérifier les informations d'identification de l'utilisateur
 * @param {string} email email
 * @param {string} password mot de passe
 */
function checkUserCredentials(email, password) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        let query = "SELECT id, lastname, firstname, email, role FROM users WHERE email = ? AND password = ?";
    
        connection.query(query, [email, password],  async(err, results, fields) => {

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);

            console.log(results);
            
            // Rejects if results does not exist
            if (results.length == 0) {
                reject("mysql error: wrong user credentials");
            } else {
                console.log("authentication successful");
                resolve(JSON.parse(JSON.stringify(results[0])));
            }
            
        });
    });
}

/**
 * @description Obtenir les emplacement du parking
 * @param {boolean} onlyAvailable filtrage sur emplacements disponibles seulement
 */
function getParkingSlots(onlyAvailable) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        let query =  "SELECT id, number, floor, occupied, userid FROM parking";

        if (onlyAvailable) {
            query = query.concat(" WHERE occupied = 0");
        }
    
        connection.query(query, async(err, results, fields) => {

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);

            console.log(results);

            resolve(JSON.parse(JSON.stringify(results)));
        });
    });
}

/**
 * @description Insérer un nouvel emplacement de parking dans la base de données
 * @param {number} number emplacement du parking
 * @param {number} floor étage
 */
function insertNewParkingSlot(number, floor) {

    /**
     * Vérifie si l'emplacement existe déjà
     * @param {number} number emplacement
     * @param {number} floor étage
     */
    let checkParkingSlotExistence = async(number, floor) => {
        return new Promise((resolve, reject) => {
            let query = "SELECT number, floor FROM parking WHERE number = ? AND floor = ?";

            connection.query(query, [number, floor], async(err, results, fields) => {
                if (err) reject(err);

                console.log(results);

                if (results.length > 0) {
                    reject("parking slot already exists");
                } else {
                    resolve();
                }
            });
        });
    };

    return new Promise(async(resolve,reject) => {

        try {
            await connectToDatabase();

            await checkParkingSlotExistence(number, floor);

            let insertQuery = "INSERT INTO parking (number, floor, occupied) VALUES (?, ?, ?)";

            connection.query(insertQuery, [number, floor, 0],  async(err, results, fields) => {
                // console.error(err);
                // console.log(results);
                // console.log(fields);

                // try {
                //     await disconnectToDatabase();
                // } catch (err) {
                //     console.error(err);
                // }
    
                if (err) reject(err);
                
                resolve();
            });

        } catch (err) {
            reject(err);
        }
    });

}

/**
 * @description Obtenir la liste des utilisateurs
 */
function getUsers() {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        let query =  "SELECT id, lastname, firstname, email, role FROM users";
    
        connection.query(query, async(err, results, fields) => {

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);

            console.log(results);

            resolve(JSON.parse(JSON.stringify(results)));
        });
    });
}

/**
 * @description Supprimer un utilisateur, il faut également mettre vacantes les places associées à l'utilisateur
 * @param {string} userid l'identifiant de l'utilisateur
 */
function deleteUser(userid) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        let query = "DELETE FROM users WHERE id = ?";
    
        connection.query(query, userid, async(err, results, fields) => {
            if (err) reject(err);
        });

        // Supprimer les places de parking associées à l'utilisateur, et les rendre disponibles
        query = "UPDATE parking SET userid = NULL, occupied = 0 WHERE userid = ?";

        connection.query(query, userid, async(err, results, fields) => {
            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);

            resolve();
        });

    });
}

/**
 * @description Mettre à jour un utilisateur
 * @param {string} userid l'identifiant de l'utilisateur
 * @param {string} lastname nom
 * @param {string} firstname prénom
 * @param {string} email email
 * @param {string} role role
 */
function updateUser(userid, lastname, firstname, email, role) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        /**
         * @description  Vérifier si le changement de mail reste unique
         * @param {string} userid 
         * @param {string} email 
         */
        let checkEmailIntegrity = (userid, email) => {
            return new Promise((resolve, reject) => {
                let integrityQuery = "SELECT id, email FROM users WHERE id <> ? AND email = ?";

                connection.query(integrityQuery, [userid, email], async(err, results, fields) => {
                    if (err) reject(err);
                    
                    if (results.length > 0) reject("Email already exists, can not update user info")
                    resolve();
                });
            });
        }

        await checkEmailIntegrity(userid, email)
            .then(() => console.log("OK email can be changed"))
            .catch(err => {
                reject(err);
            });
        
        let query = "UPDATE users SET lastname = ?, firstname = ?, email = ?, role = ?  WHERE id = ?";
    
        connection.query(query, [lastname, firstname, email, role, userid], async(err, results, fields) => {

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);

            console.log(results);

            resolve();
        });
    });
}

/**
 * @description Sélectionner un emplacement de parking. Un seul emplacement possible. il faut donc automatiquement enlever l'emplacement actuel de l'utilisateur
 * @param {string} userid l'identifiant de l'utilisateur
 * @param {number} floor étage
 * @param {number} parkingNumber emplacement parking
 */
function selectParkingSlot(userid, floor, parkingNumber) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        /**
         * @description  Vérifier si l'emplacement est toujours disponible'
         * @param {number} floor étage cible
         * @param {number} number emplacement cible
         */
        let checkAvailableParkingSlot = (floor, number) => {
            return new Promise((resolve, reject) => {
                let integrityQuery = "SELECT floor, number FROM parking WHERE floor = ? AND number = ? AND occupied = 0";

                connection.query(integrityQuery, [floor, number], async(err, results, fields) => {
                    if (err) reject(err);
                    
                    if (results.length == 0) reject("Parking slot already in use, can not select parking slot")
                    resolve();
                });
            });
        }

        await checkAvailableParkingSlot(floor, parkingNumber)
            .then(() => console.log("OK parking slot can be selected"))
            .catch(err => {
                reject(err);
            });
        
        // Libérer l'emplacement actuellement occupé
        let query = "UPDATE parking SET userid = NULL, occupied = 0 WHERE userid = ?"; 

        connection.query(query, userid, async(err, results, fields) => {
            if (err) reject(err);
        });

        // Associer l'emplacement cible
        query = "UPDATE parking SET userid = ?, occupied = 1 WHERE floor = ? AND number = ?;"; 

        connection.query(query, [userid, floor, parkingNumber], async(err, results, fields) => {

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }
           
            if (err) reject(err);

            resolve();
        });
    });
}

/**
 * @description Récupérer l'emplacement parking d'un utilisateur un emplacement de parking.
 * @param {number} userid l'identifiant de l'utilisateur
 */
function getUserParkingSlot(userid) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        let query = "SELECT floor, number FROM parking WHERE userid = ?;";
    
        connection.query(query, userid, async(err, results, fields) => {

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            if (err) reject(err);

            console.log(results);

            if (results.length == 0) {
                reject('no parking slot linked with user');
            } else {
                resolve(JSON.parse(JSON.stringify(results[0])));
            }
            
        });
    });
}

/**
 * @description Supprimer l'emplacement actuel d'un utilisateur
 * @param {string} userid l'identifiant de l'utilisateur
 */
function deleteUserParkingSlot(userid) {
    return new Promise(async(resolve,reject) => {

        await connectToDatabase();

        let query = "UPDATE parking SET userid = NULL, occupied = 0 WHERE userid = ?";
    
        connection.query(query, userid, async(err, results, fields) => {
            if (err) reject(err);

            // try {
            //     await disconnectToDatabase();
            // } catch (err) {
            //     console.error(err);
            // }

            resolve();
        });
    });
}

// connectToDatabase()
//         .then(success => console.log(success))
//         .catch(err => console.error(err));

// checkEmailExistence('lam_remi@hotmail.fr').then(results => console.log(results));

// checkUserCredentials('master@gmail.com', 'master')
//     .then(success => console.log(success))
//     .catch(err => console.error(err));


// getParkingSlots()
//     .then(success => console.log(success))
//     .catch(err => console.error(err));

// insertNewParkingSlot(31, 3)
//     .then(() => console.log("Inserted a new parking slot"))
//     .catch(err => console.error("insert parking slot failed", err));

module.exports.checkEmailExistence = checkEmailExistence;
module.exports.insertUser = insertUser;
module.exports.checkUserCredentials = checkUserCredentials;
module.exports.getParkingSlots = getParkingSlots;
module.exports.insertNewParkingSlot = insertNewParkingSlot;
module.exports.getUsers = getUsers;
module.exports.deleteUser = deleteUser;
module.exports.updateUser = updateUser;
module.exports.selectParkingSlot = selectParkingSlot;
module.exports.getUserParkingSlot = getUserParkingSlot;
module.exports.deleteUserParkingSlot = deleteUserParkingSlot;
