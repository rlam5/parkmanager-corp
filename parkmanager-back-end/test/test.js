const server = require('../app');

const chai = require('chai');
const chaiHttp = require('chai-http');
const chaiFiles = require('chai-files');
const fs = require('fs');

const expect = require('chai').expect;

chai.use(chaiHttp);
chai.use(chaiFiles);

const file = chaiFiles.file;
const dir = chaiFiles.dir;

const auth = require('../modules/authentication'); 

/** AUTHENTICATION API */
describe('TESTING AUTHENTICATION API', async() => {

    // Starts server before testing
    before(done => {
        server.app;
        done();
    });

    describe('TESTING POST /api/authentication/login endpoint', async() => {

        it('should throw error when required parameters are missing', (done) => {
            chai.request(server)
                .post('/api/authentication/login')
                .send({
                    foo: 'bar'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    expect(res.body.error).to.equal('fields required');
                    done();
                });
        });

        it('should throw error when credentials are wrong', (done) => {
            chai.request(server)
                .post('/api/authentication/login')
                .send({
                    email: 'master@gmail.com',
                    password: 'wrong'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    expect(res.body).to.contains('wrong user credentials');
                    done();
                });
        });

        it('should authenticate when credentials are correct', (done) => {
            chai.request(server)
                .post('/api/authentication/login')
                .send({
                    email: 'master@gmail.com',
                    password: 'master'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body).to.contain.keys(['token']);
                    done();
                });
        });

    });

    describe('TESTING POST /api/authentication/register endpoint', async() => {

        it('should throw error when required parameters are missing', (done) => {
            chai.request(server)
                .post('/api/authentication/register')
                .send({
                    foo: 'bar'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    expect(res.body.error).to.equal('fields required');
                    done();
                });
        });

        xit('should register a user', (done) => {
            chai.request(server)
                .post('/api/authentication/register')
                .send({
                    lastname: 'bar',
                    firstname: 'foo',
                    email: 'foo@hotmail.com',
                    password: 'password'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.message).to.equal('register successful');
                    done();
                });
        });

    });

});

/** SERVE ANGULAR APP */
describe('TESTING INDEX API (SERVE ANGULAR APP)', async() => {

    // Starts server before testing
    before(done => {
        server.app;
        done();
    });

    describe('GET Angular front end', async() => {

        it('should exist in /dist in which the angular frond-end is compiled', (done) => {
            expect(dir('dist/parkmanager-front-end')).to.exist;
            expect(file('dist/parkmanager-front-end/index.html')).to.exist;
            done();
        });

        it('should get index.html', (done) => {
            chai.request(server)
                .get('/')
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(fs.readFileSync('dist/parkmanager-front-end/index.html', "utf8")).to.equal(res.text);
                    done();
                });
        });

    });

});

/** USERS-MANAGEMENT API */
describe('TESTING USERS-MANAGEMENT API', async() => {

    /** Token jwt  with admin role to use */
    let tokenAdmin;
    /** Token jwt  with user role to use */
    let tokenUser;

    // Starts server before testing and generates JWT tokens
    before(done => {
        server.app;

        auth.authenticateUser('master@gmail.com', 'master') // contient le role d'admin
                .then(jwt => {
                    tokenAdmin = jwt;
                });

        auth.authenticateUser('user@gmail.com', 'user') // contient le role d'user
                .then(jwt => {
                    tokenUser = jwt;
                    done();
                });
    });

    describe('TESTING GET /api/manage-users endpoint', async() => {

        it('should throw error when unauthorized user attempt to use this endpoint', (done) => {
            chai.request(server)
                .get('/api/manage-users')
                .end((err, res) => {
                    expect(res.status).to.equal(403);
                    done();
                });
        });

        it('should throw error when user is not admin', (done) => {
            chai.request(server)
                .get('/api/manage-users')
                .set({'authorization': tokenUser})
                .end((err, res) => {
                    expect(res.status).to.equal(403);
                    done();
                });
        });

        it('should send users list when user is admin', (done) => {
            chai.request(server)
                .get('/api/manage-users')
                .set({'authorization': tokenAdmin})
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                });
        });

    });

    describe('TESTING POST /api/manage-users endpoint', async() => {

        it('should throw error when required parameters are missing', (done) => {
            chai.request(server)
                .post('/api/manage-users')
                .set({'authorization': tokenAdmin})
                .send({
                    foo: 'bar'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                });
        });

        xit('should register a new user', (done) => {
            chai.request(server)
                .post('/api/manage-users')
                .set({'authorization': tokenAdmin})
                .send({
                    lastname: 'test',
                    firstname: 'test',
                    email: 'test@gmail.com',
                    password: 'test',
                    role: 'user'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.message).to.equal('register successful');
                    done();
                });
        });

    });

    describe('TESTING PUT /api/manage-users endpoint', async() => {

        it('should throw error when required parameters are missing', (done) => {
            chai.request(server)
                .put('/api/manage-users')
                .set({'authorization': tokenAdmin})
                .send({
                    foo: 'bar'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                });
        });

        it('should update a user', (done) => {
            chai.request(server)
                .put('/api/manage-users')
                .set({'authorization': tokenAdmin})
                .send({
                    id: 1,
                    lastname: 'master',
                    firstname: 'master',
                    email: 'master@gmail.com',
                    password: 'master',
                    role: 'admin'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.message).to.equal('update user info successful');
                    done();
                });
        });

    });

});

/** PARKING-MANAGEMENT API */
describe('TESTING PARKING-MANAGEMENT API', async() => {

    /** Token jwt  with admin role to use */
    let tokenAdmin;
    /** Token jwt  with user role to use */
    let tokenUser;

    // Starts server before testing and generates JWT tokens
    before(done => {
        server.app;

        auth.authenticateUser('master@gmail.com', 'master') // contient le role d'admin
                .then(jwt => {
                    tokenAdmin = jwt;
                });

        auth.authenticateUser('user@gmail.com', 'user') // contient le role d'user
                .then(jwt => {
                    tokenUser = jwt;
                    done();
                });
    });

    describe('TESTING GET /api/manage-parking/available-parking endpoint', async() => {

        it('should throw error when unauthorized user attempt to use this endpoint', (done) => {
            chai.request(server)
                .get('/api/manage-parking/available-parking')
                .end((err, res) => {
                    expect(res.status).to.equal(403);
                    done();
                });
        });

        it('should send available parking', (done) => {
            chai.request(server)
                .get('/api/manage-parking/available-parking')
                .set({'authorization': tokenUser})
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                });
        });

    });

    describe('TESTING PUT /api/manage-parking/select-parking endpoint', async() => {

        it('should throw error when unauthorized user attempt to use this endpoint', (done) => {
            chai.request(server)
                .put('/api/manage-parking/select-parking')
                .end((err, res) => {
                    expect(res.status).to.equal(403);
                    done();
                });
        });

        it('should throw error when required parameters are missing', (done) => {
            chai.request(server)
                .put('/api/manage-parking/select-parking')
                .set({'authorization': tokenAdmin})
                .send({
                    id: 1,
                    foo: 'bar'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                });
        });

        it('should throw error when the user is not trustworthy', (done) => {
            chai.request(server)
                .put('/api/manage-parking/select-parking')
                .set({'authorization': tokenAdmin}) 
                .send({
                    id: 1000, // l'id de l'admin est 1
                    floor: 3,
                    number: 10
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                });
        });

    });

    describe('TESTING GET /api/manage-parking/select-parking endpoint', async() => {

        it('should send user parking slot', (done) => {
            chai.request(server)
                .get('/api/manage-parking/select-parking/1')
                .set({'authorization': tokenAdmin})
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                });
        });

    });

    describe('TESTING GET /api/manage-parking endpoint', async() => {

        it('should send all parking slots', (done) => {
            chai.request(server)
                .get('/api/manage-parking')
                .set({'authorization': tokenAdmin})
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                });
        });

    });

    describe('TESTING POST /api/manage-parking/parking-slot endpoint', async() => {

        it('should throw error when missing required parameters', (done) => {
            chai.request(server)
                .post('/api/manage-parking/parking-slot')
                .set({'authorization': tokenAdmin})
                .send({
                    foo: 'bar'
                })
                .end((err, res) => {
                    expect(res.status).to.equal(422);
                    done();
                });
        });

        it('should throw error when wrong parameters type', (done) => {
            chai.request(server)
                .post('/api/manage-parking/parking-slot')
                .set({'authorization': tokenAdmin})
                .send({
                    number: 'bar',
                    floor: 3
                })
                .end((err, res) => {
                    expect(res.status).to.equal(422);
                    done();
                });
        });

        it('should throw error when parameters are invalid', (done) => {
            chai.request(server)
                .post('/api/manage-parking/parking-slot')
                .set({'authorization': tokenAdmin})
                .send({
                    number: 40,
                    floor: 6
                })
                .end((err, res) => {
                    expect(res.status).to.equal(422);
                    done();
                });
        });

    });

});