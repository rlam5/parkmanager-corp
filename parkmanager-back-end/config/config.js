const path = require('path');

const config = {
    mysql: {
        host: 'localhost',
        port: 3306,
        user: 'root',
        pwd: 'root',
        database: 'park_manager'
    },
    secret: 'mah5fseww9', // secret utilisé pour encoder les JWT Token
}

module.exports = config;