const express = require('express');
const router = express.Router();
const usersManagement = require('../modules/users-management');
const authCheck = require('../libs/check-authorization');

/** route middleware pour vérifier le token JWT */
router.use(function(req, res, next) {
    authCheck.checkAuthorization(req, res, next);
});

/** route middleware pour vérifier le rôle admin de l'utilisateur */
router.use(function(req, res, next) {
    authCheck.isAdmin(req, res, next);
});

/** GET récupérer la liste des utilisateurs */
router.get('/', async(req, res, next) => {
  await usersManagement.getUsers()
          .then(usersList => {
              console.log(usersList);
              return res.status(200).json(usersList);
          })
          .catch(err => {
              console.error('err', err)
              return res.status(400).json(err);
          });
});

/** POST créer un nouvel utilisateur */
router.post('/', async(req, res, next) => {

    if (!req.body.lastname || !req.body.firstname || !req.body.email || !req.body.password || !req.body.role) {
        console.log('register user error: fields required');
        return res.status(400).json({error: 'fields required'});
    }

    await usersManagement.registerUser(req.body.lastname, req.body.firstname, req.body.email, req.body.password, req.body.role)
                .then(() => {
                    return res.status(200).json({"message" : "register successful"});
                })
                .catch(err => {
                    console.error('err', err)
                    return res.status(400).json(err);
                });
                
});

/** PUT mettre à jour un utilisateur */
router.put('/', async(req, res, next) => {

    if (!req.body.id || !req.body.lastname || !req.body.firstname || !req.body.email || !req.body.role) {
        console.log('update user info error: fields required');
        return res.status(400).json({error: 'fields required'});
    }

    await usersManagement.updateUser(req.body.id, req.body.lastname, req.body.firstname, req.body.email, req.body.role)
            .then(() => {
                console.log("update user info succesful");
                return res.status(200).json({"message" : "update user info successful"});
            })
            .catch(err => {
                console.error('err', err)
                return res.status(400).json(err);
            });
});

/** DELETE supprimer un utilisateur */
router.delete('/:userid', async(req, res, next) => {

    let userid = req.params.userid;

    await usersManagement.deleteUser(userid)
            .then(() => {
                console.log("delete user successful");
                return res.status(200).json({message: "delete user successful"});
            })
            .catch(err => {
                console.error('err', err)
                return res.status(422).json(err);
            });
});

module.exports = router;
