const express = require('express');
const router = express.Router();
const parking = require('../modules/parking-management');
const authCheck = require('../libs/check-authorization');

/** route middleware pour vérifier le token JWT */
router.use(function(req, res, next) {
    authCheck.checkAuthorization(req, res, next);
});

/** GET pour récupérer les emplacements disponibles du parking */
router.get('/available-parking', async(req, res, next) => {
    await parking.getParkingSlots(true)
                    .then(parkingSlots => {
                        console.log(parkingSlots);
                        return res.status(200).json(parkingSlots);
                    })
                    .catch(err => {
                        console.error('err', err)
                        return res.status(400).json(err);
                    });
});

/** PUT pour sélectionner un emplacement parking pour l'utilisateur */
router.put('/select-parking', async(req, res, next) => {

    if (!req.body.id || !req.body.floor || !req.body.number) {
        console.error('select parking for user error: fields required');
        return res.status(400).json({error: 'fields required'});
    }

    if (req.body.id !== req.decoded.userid) {
        console.error('select parking for user error: user not trustworthy');
        return res.status(400).json({error: 'user not trustworthy'});
    }

    await parking.selectParkingSlot(req.body.id, req.body.floor, req.body.number)
                    .then(() => {
                        return res.status(200).json({"message" : "select parking slot for user successful"});
                    })
                    .catch(err => {
                        console.error('err', err)
                        return res.status(400).json(err);
                    });
});

/** GET pour récupérer l'emplacement d'un utilisateur */
router.get('/select-parking/:userid', async(req, res, next) => {

    if (!req.params.userid) {
        console.error('select parking for user error: params required');
        return res.status(400).json({error: 'params required'});
    }

    if (Number(req.params.userid) !== req.decoded.userid) {
        console.error('select parking for user error: user not trustworthy');
        return res.status(400).json({error: 'user not trustworthy'});
    }

    await parking.getUserParkingSlot(req.params.userid)
                    .then(userParkingSlot => {
                        console.log(userParkingSlot);
                        return res.status(200).json(userParkingSlot);
                    })
                    .catch(err => {
                        console.error('err', err)
                        return res.status(400).json(err);
                    });

});

/** DELETE pour supprimer l'emplacement d'un utilisateur */
router.delete('/select-parking/:userid', async(req, res, next) => {

    if (!req.params.userid) {
        console.error('delete parking for user error: params required');
        return res.status(400).json({error: 'params required'});
    }

    if (Number(req.params.userid) !== req.decoded.userid) {
        console.error('delete parking for user error: user not trustworthy');
        return res.status(400).json({error: 'user not trustworthy'});
    }

    await parking.deleteUserParkingSlot(req.params.userid)
                    .then(() => {
                        console.log("delete parking slot for user successful");
                        return res.status(200).json({"message" : "delete parking slot for user successful"});
                    })
                    .catch(err => {
                        console.error('err', err)
                        return res.status(400).json(err);
                    });

});

/** route middleware pour vérifier le rôle admin de l'utilisateur */
router.use(function(req, res, next) {
    authCheck.isAdmin(req, res, next);
});

/** GET pour récupérer les emplacements du parking */
router.get('/', async(req, res, next) => {
    await parking.getParkingSlots()
                    .then(parkingSlots => {
                        console.log(parkingSlots);
                        return res.status(200).json(parkingSlots);
                    })
                    .catch(err => {
                        console.error('err', err)
                        return res.status(400).json(err);
                    });
});

/** POST pour ajouter un nouvel emplacement de parking */
router.post('/parking-slot', async(req, res, next) => {

    // Verifie l'existence des paramètres
    if (!req.body.number || !req.body.floor) {
        return res.status(422).json({
            message: "missing required parameters"
        });
    }

    // Verifie parameters type
    if (!(typeof Number(req.body.number) === 'number') || !(typeof Number(req.body.floor) === 'number')) {
        return res.status(422).json({
            message: "wrong parameters type"
        });
    }

    // Verifie si les valeurs sont valides (30 emplacements max, 4 étages max)
    if ( !(req.body.number >= 1 && req.body.number <= 30) || !(req.body.floor >= 1 && req.body.floor <= 4) ) {
        return res.status(422).json({
            message: "invalid parameters value"
        });
    }

    await parking.insertParkingSlot(Number(req.body.number), Number(req.body.floor))
        .then(() => {
            console.log("Inserted new parking slot in database");
            return res.status(200).json({message: "insert new parking slot successful"});
        })
        .catch(err => {
            console.error('err', err)
            return res.status(422).json({message: err});
        });
        
});

module.exports = router;
