const express = require('express');
const router = express.Router();
const auth = require('../modules/authentication');

/** POST pour s'authentifier */
router.post('/login', async(req, res, next) => {

    // Checks required parameters
    if(!req.body.email || !req.body.password) {
      console.log('authentication error: fields required');
      return res.status(400).json({error: 'fields required'});
    }

    await auth.authenticateUser(req.body.email, req.body.password)
            .then(token => {
                // console.log(token);
                return res.status(200).json({"token" : token});
            })
            .catch(err => {
                console.error('err', err)
                return res.status(400).json(err);
            });
  
});

/** POST pour inscription d'un nouvel utilisateur */
router.post('/register', async(req, res, next) => {

    if (!req.body.lastname || !req.body.firstname || !req.body.email || !req.body.password) {
        console.log('authentication error: fields required');
        return res.status(400).json({error: 'fields required'});
    }

    await auth.registerUser(req.body.lastname, req.body.firstname, req.body.email, req.body.password)
                .then(() => {
                    return res.status(200).json({"message" : "register successful"});
                })
                .catch(err => {
                    console.error('err', err)
                    return res.status(400).json(err);
                });

});
  
module.exports = router;
  