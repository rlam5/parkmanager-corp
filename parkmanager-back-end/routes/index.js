const express = require('express');
const router = express.Router();
const path = require('path');

/** GET ANGULAR distribution. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../dist/parkmanager-front-end/index.html'));
});

module.exports = router;
