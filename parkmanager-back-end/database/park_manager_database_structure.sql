-- Creation de la base de donnees park_manager
CREATE DATABASE IF NOT EXISTS park_manager;

USE park_manager;

-- Creation de la table users
CREATE TABLE IF NOT EXISTS users (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	lastname TEXT,
	firstname TEXT,
	email TEXT,
	password TEXT,
	role TEXT
);

-- Creation de la table parking
CREATE TABLE IF NOT EXISTS parking (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	number INT,
	floor INT,
	occupied BOOLEAN,
	userid INT
);

-- Insertion du profil administrateur
INSERT INTO users (id, lastname, firstname, email, password, role) VALUES(1, 'master', 'master', 'master@gmail.com', 'master', 'admin') 
	ON DUPLICATE KEY UPDATE    
		lastname='master', firstname='master', email='master@gmail.com', password='master', role='admin';
		
-- Insertion du profil utilisateur
INSERT INTO users (id, lastname, firstname, email, password, role) VALUES(2, 'user', 'user', 'user@gmail.com', 'user', 'user') 
	ON DUPLICATE KEY UPDATE    
		lastname='user', firstname='user', email='user@gmail.com', password='user', role='user';

-- Autoriser les updates et les deletes
SET SQL_SAFE_UPDATES = 0;

-- Remettre authentification mysql_native_password pour que le serveur express JS puisse se connecter a la base. (Changer le nom de l'hote et le password selon votre config host = root et pwd = root ici)
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';